﻿This file exists to ensure that the enclosing directory is committed to git. It's only here to make
local development easier. In production, the actual logging directory will probably be different.