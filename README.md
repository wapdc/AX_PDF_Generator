This project implements a basic http endpoint that can accept a search parameter
and return an ApplicationXtender document. It relies on application specific 
data structures for ApplicationXtender.

It works with AX 7 and files inserted as PDF, TIFF, JPEG, GIF and BMP. It fails 
when inserting PNG images because AX seems to be adding 4 lines of header info
to the files.

Copyright (C) 2017  James Gutholm

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.