﻿/**
    Copyright 2017, James Gutholm, Washington Public Disclosure Commission

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System.Collections.Generic;
using log4net;
using System.Configuration;
using System.Data.SqlClient;

namespace ReportImager
{
    /// <summary>
    /// Utility class to manage ApplicationXtender documents.
    /// </summary>
    public class AXDocuments
    {
        private static readonly ILog Logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static string connectionString = ConfigurationManager.ConnectionStrings["IMAGE"].ConnectionString;
        //private static string axBasePath = getAxBasePath();


        /**
         * Defines the possible searches. To add a new search type, define a new key/val such that 
         * key = querystring parameter and value = the name of the item in the app configuration
         * that defines the single parameter query to run.
         * 
         * The batchnumber and repno are the same query. For some reason, both have been used as 
         * parameters. This behavior was preserved when the code was refactored.
         */
        public static Dictionary<string, string> searchTypes
            = new Dictionary<string, string> {
                { "batchnumber", "axRepnoQuery" },
                { "repno", "axRepnoQuery" },
                { "docid", "axDocidQuery" },
                { "filerid_election_year", "axFilerIdQuery" }
            };



        /**
         * Defines the possible searches for authenticated requests. To add a new search type, define a new key/val such that 
         * key = querystring parameter and value = the name of the item in the app configuration
         * that defines the single parameter query to run.
         * 
         * The batchnumber and repno are the same query. For some reason, both have been used as 
         * parameters. This behavior was preserved when the code was refactored.
         */
        public static Dictionary<string, string> privilegedSearchTypes
            = new Dictionary<string, string> {
                { "batchnumber", "axPrivilegedRepnoQuery" },
                { "repno", "axPrivilegedRepnoQuery" },
                { "docid", "axPrivilegedDocidQuery" },
            };


        /// <summary>
        /// Returns a list of pages that need to be rendered. The list is in the correct render
        /// order. The list may be empty but will not be null. 
        /// </summary>
        /// <param name="searchParameter">Must match a key in the searchTypes dictionary.</param>
        /// <param name="searchValue">The underlying query may cast this parameter to into or 
        /// another type, depending on the data column being queried. The caller is responsible for
        /// passing this string in a form that it can be cast to the correct database type.</param>
        /// <returns>The list of pages that need to be rendered or an empty list.</returns>
        public static List<AXPage> Find(string searchParameter, string searchValue, bool privileged)
        {
            List<AXPage> pages = new List<AXPage>();

            string searchQueryKey = privileged ? privilegedSearchTypes[searchParameter] : searchTypes[searchParameter];
            Logger.Info($@"Using search query key {searchQueryKey}.");

            string searchQuery =
                ConfigurationManager.AppSettings[searchQueryKey];
            Logger.Info($@"Using search query {searchQueryKey} with parameter value {searchValue}.");

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            using (SqlCommand sqlCommand = new SqlCommand(searchQuery, sqlConnection))
            {
                sqlCommand.Parameters.AddWithValue("param", searchValue);
                sqlConnection.Open();
                using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                {
                    while (sqlDataReader.Read())
                    {
                        int docid = sqlDataReader.GetInt32(sqlDataReader.GetOrdinal("docid"));
                        string fullyQualifiedPath = 
                            sqlDataReader.GetString(sqlDataReader.GetOrdinal("fullyQualifiedPath"));

                        Logger.Debug($@"Adding page with docid: {docid} and path: {fullyQualifiedPath}.");

                        pages.Add(new AXPage(docid, fullyQualifiedPath));
                        
                    }
                }
            }

            Logger.Info($@"Found {pages.Count} pages matching request.");

            return pages;
        }


    }
}