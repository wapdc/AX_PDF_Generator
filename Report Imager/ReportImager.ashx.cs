﻿/**
    Copyright 2017, James Gutholm, Washington Public Disclosure Commission

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using log4net;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace ReportImager
{
    /// <summary>
    /// Generates a PDF document based on the querystring search parameter. It is possible that a 
    /// search by repno or batchnumber will return more than one document from AX. In these cases
    /// the pages are ordered by the docid and page number so the lowest docid comes first with all
    /// pages in the expected order.
    /// 
    /// The handler expects one of:
    /// ?batchnumber=abc
    /// ?repno=n
    /// ?docid=n
    /// 
    /// Batchnumber and repno both search the same field in AX.
    /// </summary>
    public class ReportImager : IHttpHandler
    {
        private static readonly int validSeconds = Int32.Parse(ConfigurationManager.AppSettings["privilegedQuerySignatureTimeout"]);
        private static readonly ILog Logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        /// <summary>
        /// Querystring must have a parameter that matches one of the keys in the searchTypes dictionary. 
        /// The first one found is used for the query and the rest are just ignored.
        /// 
        /// Search context.Request.QueryString for any in single parameter defined by AXDocuments.searchTypes
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {

            bool privileged = false;
            Logger.Info($@"Processing request for page {context.Request.RawUrl}.");
            string searchType;
            string searchValue;
            try
            {
                if (context.Request.QueryString.AllKeys.Contains("signature"))
                {
                    if (!validateSignedData(context.Request.QueryString["signature"], context.Request.QueryString["data"]))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                        return;
                    }

                    var parameters = getCollectionFromBase64EncodedData(context.Request.QueryString["data"]);

                    if (!validateTimeStamp(parameters, validSeconds))
                    {
                        context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                        return;
                    }
                    searchType = (from q in parameters.AllKeys
                                  join s in AXDocuments.privilegedSearchTypes.Keys
                                  on q equals s
                                  select q).FirstOrDefault();

                    searchValue = parameters[searchType];
                    privileged = true;
                    Logger.Info($@"Privileged search for {searchType}.");
                }
                else
                {
                    searchType = (from q in context.Request.QueryString.AllKeys
                                  join s in AXDocuments.searchTypes.Keys
                                  on q equals s
                                  select q).FirstOrDefault();

                    searchValue = context.Request.QueryString[searchType];
                }



                Logger.Debug($@"Using searchType: {searchType}.");
                Logger.Debug($@"Using searchValue: {searchValue}.");

                if (searchType != null && searchValue != null)
                {
                    List<AXPage> pages = AXDocuments.Find(searchType, searchValue, privileged);
                    if (pages.Count() > 0)
                    {

                        byte[] newPdf = Generator.Generate(pages);

                        string fileName = $@"{searchValue}.pdf";

                        context.Response.ContentType = "application/pdf";
                        context.Response.AppendHeader(
                            "Content-Disposition",
                            $@"inline; filename={fileName}");
                        context.Response.AppendHeader("Access-Control-Allow-Origin", "*");

                        Logger.Info($@"Sending {newPdf.Count()} bytes to client as file {fileName}");
                        context.Response.BinaryWrite(newPdf);

                    }
                    else
                    {
                        Logger.Error($@"No pages matching request for page {context.Request.RawUrl}");
                        context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                        context.Response.SubStatusCode = 0;
                    }

                }
                else
                {
                    Logger.Error($@"Error generating request for page {context.Request.RawUrl}. Invalid or missing querystring parameters.");
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
            }
            catch (Exception e)
            {
                Logger.Error($@"Error generating request for page {context.Request.RawUrl}. Error: {e.Message}.");
                Logger.Error(e.StackTrace);
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }


        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private static bool validateSignedData(string signature, string data)
        {
            var dir = HttpContext.Current.Server.MapPath("~");

            X509Certificate2 cert = new X509Certificate2(dir + "/pdc-self-signed.crt");
            RSACryptoServiceProvider csp = (RSACryptoServiceProvider)cert.PublicKey.Key;
            SHA256Managed sha256 = new SHA256Managed();
            byte[] dataBytes = Convert.FromBase64String(data);
            byte[] hash = sha256.ComputeHash(dataBytes);
            byte[] signatureBytes = Convert.FromBase64String(signature);

            return csp.VerifyHash(hash, CryptoConfig.MapNameToOID("SHA256"), signatureBytes);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private static NameValueCollection getCollectionFromBase64EncodedData(string base64Data)
        {
            var dataBytes = Convert.FromBase64String(base64Data);
            var dataJson = Encoding.UTF8.GetString(dataBytes, 0, dataBytes.Length);
            var dictionary = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(dataJson);
            var nameValueCollection = new NameValueCollection(dictionary.Count);
            foreach (var k in dictionary)
            {
                nameValueCollection.Add(k.Key, k.Value);
            }
            Logger.Info($@"Priviliged search contains {nameValueCollection.Count} parameters.");
            return nameValueCollection;
        }

        private static bool validateTimeStamp(NameValueCollection collection, int validSeconds)
        {
            var currentTimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds();
            var signedTimeStamp = long.Parse(collection.Get("timestamp"));
            var difference = Math.Abs(currentTimeStamp - signedTimeStamp);
            
            if(difference <= validSeconds)
            {
                Logger.Info($@"Validaed timestamp with difference of {difference} seconds.");
            }
            else
            {
                Logger.Error($@"Failed to validate timestamp {signedTimeStamp}, current timestamp = {currentTimeStamp} seconds.");
            }

            return (difference <= validSeconds) ? true : false;            
        }
    }
}