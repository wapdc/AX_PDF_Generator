﻿/**
    Copyright 2017, James Gutholm, Washington Public Disclosure Commission

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;

using FileSignatures;
using log4net;
using FileSignatures.Formats;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace ReportImager
{
    public class Generator
    {
        private static readonly ILog Logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        /// <summary>
        /// Given a list of AXpage, renders all pages as a single Pdf document. The underlying 
        /// files must be pdf or any of the image formats supported by 
        /// iTextSharp.text.Image.GetInstance. Image files are rendered at letter size.
        /// </summary>
        /// <param name="pages">List of AXpages to be rendered.</param>
        /// <returns>An array of bytes representing the pdf.</returns>
        public static byte[] Generate(List<AXPage> pages)
        {
            Logger.Info($@"Generating PDF bytes for {pages.Count} pages.");
            FileFormatInspector inspector = new FileFormatInspector();

            using (MemoryStream pdfStream = new MemoryStream())
            {
                using (Document document = new Document())
                {
                    /**
                     * Using PdfSmartCopy is documented as the prefered way to merge documents. It
                     * only supports adding Pdf documents so any tiff or other image pages need to
                     * be converted into a Pdf byte array before being added to the document.
                     */
                    using (var copy = new PdfSmartCopy(document, pdfStream))
                    {
                        document.Open();
                        foreach (AXPage page in pages)
                        {
                            Logger.Debug($@"Attempting to generate page for docid: {page.docid} using path: {page.fullyQualifiedPath}.");

                            FileFormat format = GetFileType(inspector, page);
                            Logger.Debug($@"File format for path: {page.fullyQualifiedPath} identified as {format.MediaType}");

                            Byte[] pdfBytes = null;



                            if (format is FileSignatures.Formats.Pdf)
                            {
                                Logger.Debug($@"Getting PDF file bytes for docid: {page.docid} from path: {page.fullyQualifiedPath.FullName}.");
                                pdfBytes = ReadAllBytesSharedAccess(page.fullyQualifiedPath.FullName);
                            }
                            else if (format is FileSignatures.Formats.Image)
                            {
                                Logger.Debug($@"Converting image file for docid: {page.docid} from path: {page.fullyQualifiedPath.FullName} into PDF bytes.");
                                pdfBytes = GetPdfFromImage(page.fullyQualifiedPath.FullName);
                            }
                            else
                            {
                                Logger.Error($@"Found unsupported file format for docid: {page.docid} identified as {format.MediaType}. Must be one of PDF or Image.");
                                throw (new Exception("Found unsupported file format"));
                            }

                            using (var reader = new PdfReader(pdfBytes))
                            {
                                copy.AddDocument(reader);
                            }
                        }
                        document.Close();
                    }
                }
                return pdfStream.ToArray();
            }
        }

        /// <summary>
        /// A method to read all bytes from a file. Basically does the same as File.ReadAllBytes()
        /// but it uses shared access to avoid locks. There are still problems with AX taking an 
        /// exclusive lock on files when it has them open but this should at least help concurrency 
        /// in the web app.
        /// </summary>
        /// <param name="file">The path to the file</param>
        /// <returns>Byte array containg content of the file.</returns>
        private static byte[] ReadAllBytesSharedAccess(string file)
        {
            byte[] pdfBytes;
            using (System.IO.FileStream fs =
                                 System.IO.File.Open(file,
                                 FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                pdfBytes = new byte[fs.Length];
                int numBytesToRead = Convert.ToInt32(fs.Length);
                int numBytesRead = 0;

                while (numBytesToRead > 0)
                {
                    // Read may return anything from 0 to numBytesToRead.
                    int n = fs.Read(pdfBytes, numBytesRead, numBytesToRead);

                    // Break when the end of the file is reached.
                    if (n == 0)
                        break;

                    numBytesRead += n;
                    numBytesToRead -= n;
                }

            }

            return pdfBytes;
        }

        /// <summary>
        /// Gets the format of the file based on file magic.
        /// </summary>
        /// <param name="inspector"></param>
        /// <param name="page"></param>
        /// <returns>The image format.</returns>
        private static FileFormat GetFileType(FileFormatInspector inspector, AXPage page)
        {
            FileFormat format = null;
            try
            {
                using (FileStream sourceStream = new FileStream(page.fullyQualifiedPath.FullName, FileMode.Open, FileAccess.Read))
                {
                    format = inspector.DetermineFileFormat(sourceStream);
                }
            }
            catch (Exception e)
            {
                // This catch and rethrow was added because iText is masking the exception at a higher
                // level by throwing a document not open exception.
                Logger.Error($@"Could not determine file format for docid: {page.docid} due to error: { e.Message}.");
                throw e;
            }

            if (format == null)
            {
                Logger.Error($@"Could not determine file format for docid: {page.docid}");
                throw (new Exception("Could not determine file format for docid: {page.docid}"));
            }

            return format;
        }


        /// <summary>
        /// Converts an image from a file to a PDF as an array of bytes. The image file must be one
        /// of the types supported by iTextSharp. Multipage images such as multi-page TIFF are not
        /// supported. Regular single page TIFF is as well as jpeg, bmp, png, etc.
        /// </summary>
        /// <param name="imagePath">Path to image file</param>
        /// <returns>An array of bytes representing the pdf.</returns>
        private static byte[] GetPdfFromImage(string imagePath)
        {
            //var img = iTextSharp.text.Image.GetInstance(imagePath);

            byte[] imageData = ReadAllBytesSharedAccess(imagePath);

            var img = iTextSharp.text.Image.GetInstance(imageData);

            img.ScaleToFit(PageSize.LETTER);
            img.SetAbsolutePosition(0, 0);

            //Simple image to PDF
            using (var memoryStream = new MemoryStream())
            {
                using (var document = new Document(PageSize.LETTER, 0, 0, 0, 0))
                {
                    using (var w = PdfWriter.GetInstance(document, memoryStream))
                    {
                        document.Open();
                        document.Add(img);
                        document.Close();
                    }
                }

                return memoryStream.ToArray();
            }
        }


    }
}