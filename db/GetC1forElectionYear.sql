IF OBJECT_ID('GetC1PCforElectionYear') IS NOT NULL
  BEGIN
    DROP FUNCTION GetC1PCforElectionYear
  END
GO 

IF OBJECT_ID('GetC1forElectionYear') IS NOT NULL
  BEGIN
    DROP FUNCTION GetC1forElectionYear
  END
GO 

CREATE FUNCTION GetC1forElectionYear
(@param nvarchar(max))
RETURNS @doctable TABLE (docid int, fullyQualifiedPath varchar(MAX), minpage int)
AS
BEGIN
DECLARE @filer_id varchar(MAX)
DECLARE @election_year int
DECLARE @delimiter varchar(MAX)
DECLARE @delimiter_len int
set @delimiter = ':|:'
set @delimiter_len = len(@delimiter)

set @filer_id = SUBSTRING(@param, 1, PATINDEX('%' + @delimiter + '%', @param) - 1)
set @election_year = SUBSTRING(@param,PATINDEX('%' + @delimiter + '%', @param) + @delimiter_len, 8000)

	DECLARE @ret_docid INT

	SELECT top 1 @ret_docid = docid
	FROM ae_dt1
	WHERE 
	(
		((field3 = 'C1' OR field3 = 'C1 AMENDED') and field4 = @election_year) -- Candidates
	or	
		((field3 = 'SURPLUS ACCT C1' OR field3 = 'SURPLUS ACCT C1 AMENDED') and field2 like '%*%') -- Surplus always most recent
	or	
	    ((field3 = 'C1PC' OR field3 = 'C1PC AMENDED') and field4 <= @election_year) -- Committees, inc. continuing, any earlier year. Not 100% correct for single year committees
	)
	AND field2 = @filer_id
	ORDER BY
		-- Order by date, amended, efiled, repno when efile, date when batch is a date
		field6 DESC,
		CASE
			WHEN field3 like '%AMENDED' THEN 1
			ELSE 0
		END DESC,
		CASE
			WHEN field13 = 'EFILE' THEN 1
			ELSE 0
		END DESC,
		CASE
			WHEN ISNUMERIC(field10) = 1 THEN CAST(field10 AS DECIMAL)
			ELSE -1
		END DESC,
		CASE
			WHEN ISDATE(field10) = 1 THEN CAST(field10 AS DATETIME)
			ELSE CAST('19000101' AS DATETIME)
		END DESC,
		docid DESC

INSERT INTO @doctable select p.docid, p.fullyQualifiedPath, p.minpage 
                from ax_document_page_paths p 
                join ae_dt1 d on p.docid = d.docid
                where ltrim(d.field3) not like 'F1%'
                and p.docid = @ret_docid 
return
END
GO


